# Comments

## 1. Routing logic

_Please add any notes for task 2 here_

## 2. Completing the form

_Please add any notes for task 2 here_

## 3. Separating the submitter information from the `GuestbookEntry`

_Please add any notes for task 3 here_

## 4. Update an entry

_Please add any notes for task 4 here_

## 5. Generate an hourly report

_Please add any notes for task 5 here_

## 6. React to an entry being deleted

_Please add any notes for task 6 here_
